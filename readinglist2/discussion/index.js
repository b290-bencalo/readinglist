// console.log("TGIF");

// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.
/* 
    Syntax:
        fetch("url")
            url - this is the address which the request is to be made and source where the response will come from (endpoint)
            options - array or properties that contains the HTTP method, body of request and headers.
*/

// Get Post Data / Retrieve/Read Function

fetch("https://jsonplaceholder.typicode.com/posts")
  .then((res) => res.json())
  .then((data) => showPosts(data));

// VIEW POST - to display each post from JSON placeholder
const showPosts = (posts) => {
  let postEntries = "";
  // To iterate each posts from the API
  posts.forEach((post) => {
    postEntries += `
    <div id="post-${post.id}">
      <h3 id="post-title-${post.id}">${post.title}</h3>
      <p id="post-body-${post.id}">${post.body}</p>
      <button onclick="editPost('${post.id}')">Edit</button>
      <button onclick="deletePost('${post.id}')">Delete</button>
    </div>
    `;
  });
  // To check what is stored in the postEntries variables.
  console.log(postEntries);
  // To assign the value of postEntries to the element with "div-post-entries" id.
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

/*
	Mini-Activity:
		Retrieve a single post from JSON API and print it in the console.
*/
fetch("https://jsonplaceholder.typicode.com/posts/1")
  .then((res) => res.json())
  .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/2")
  .then((res) => res.json())
  .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/")
  .then((res) => res.json())
  .then((json) => {
    const filteredPosts = json.filter((post) => post.id === 1 || post.id === 2);
    console.log(filteredPosts);
  });

// Post Data / Create Function

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  // Prevents the page from reloading. Also prevents the default behavior of our event.
  e.preventDefault();
  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: document.querySelector("#txt-title").value,
      body: document.querySelector("#txt-body").value,
      userId: 290,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
      alert("Post Successfully Added!");
    });
  document.querySelector("#txt-title").value = null;
  document.querySelector("#txt-body").value = null;
});

// EDIT POST DATA / EDIT BUTTON

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;

  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// Update Post Data / PUT Method

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();
  let id = document.querySelector("#txt-edit-id").value;
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: "PUT",
    body: JSON.stringify({
      id: id,
      // the value will be from the value of input fields or HTML elements
      title: document.querySelector("#txt-edit-title").value,
      body: document.querySelector("#txt-edit-body").value,
      userId: 290,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
      alert("Post successfully updated");
    });
  console.log(id);
  // Resetting disabled attribute for button
  document.querySelector("#txt-edit-title").value = null;
  document.querySelector("#txt-edit-body").value = null;
  // the value will be from the value of input fields or HTML elements
  document.querySelector("#btn-submit-update").setAttribute("disabled", true);
});

const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: "DELETE",
  })
    .then((response) => {
      if (response.ok) {
        console.log("Post deleted successfully");
        // Remove the element from the DOM
        const postElement = document.getElementById(`post-${id}`);
        if (postElement) {
          postElement.remove();
          console.log(`Element with ID ${id} removed from the DOM`);
        }
      } else {
        console.log("Failed to delete post");
        // Handle the error or display an error message
      }
    })
    .catch((error) => {
      console.log("Error occurred while deleting post", error);
      // Handle the error or display an error message
    });
};
